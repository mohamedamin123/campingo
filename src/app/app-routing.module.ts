import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminlayoutComponent } from './layouts/adminlayout/adminlayout.component';
import { UserlayoutComponent } from './layouts/userlayout/userlayout.component';

const routes: Routes = [
  {path:'',component:UserlayoutComponent,children:[
    {path:'',loadChildren:()=>import('./views/user/home/home.module').then(m=>m.HomeModule)},
    {path:'signin',loadChildren:()=>import('./views/user/signin/signin.module').then(m=>m.SigninModule)},
    {path:'signup',loadChildren:()=>import('./views/user/signup/signup.module').then(m=>m.SignupModule)}
  ]},
  {path:'admin',component:AdminlayoutComponent,children:[
    {path:'dashboard',loadChildren:()=>import('./views/admin/dashboard/dashboard.module').then(m=>m.DashboardModule)},
    {path:'adminlogin',loadChildren:()=>import('./views/admin/adminlogin/adminlogin.module').then(m=>m.AdminloginModule)}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
